# lockscreen_prank

simple bash script using a picture of your current active workspace as lockscreen

## config

Feel free to set the pic_folder to a folder to your liking \
(may even be a temporary one).\
\
Comment out the wartermark section to prank folks by not indicating
the screen is locked with a watermark \
or set it to be whatever picture you like there. \

## usage

dependencies:\

- scrot 
- i3lock 
- imagemagick
\
There is no official release intended. \
\
If you'd like to use this script make sure the dependencies are met.\
Eg. by running following command on debian systems: \
\
`sudo apt install scrot i3lock imagemagick`\
\
Thereafter just copy the script somewhere in your $PATH (eg. /usr/bin or ~/bin) 
and make it executable.\
Eg. by running following command:\
\
`chmod +x ~/bin/lockscreen_prank`\
\
It is highly recommended to review the scirpt befor first running it! \
By default screenshot to be used as lockscreen is saved to \
**/home/$USER/.local/share/lockscreen_prank/assets**\
and allthough this folder will be created (if not present) on the first run,\
the watermark will also be taken from there.\
In order not to error out, if no watermark is found a transparent 4x4px watermark.png
will be created there too.\
I recommend replacing this with your own, the one found here in assets folder \
or comment out the water section alltogether.\
\

## TO DO
\
- get a grasp of using makefile or debpkg to enable proper installation\
\

(c)2022 WTFPL by naik <nachtlicht@tuta.io>


